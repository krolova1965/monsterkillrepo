using Code.gameConfiguration;

namespace Code.GameField
{
    public class GameFieldLevelControl
    {
        private GameFieldConfiguration _gameFieldConfiguration;
        private int _currentLevel;
        private int _killedUnitsForNextLevel;
        private int _killedUnitsInLevel;

        public GameFieldLevelControl(GameFieldConfiguration gameFieldConfiguration)
        {
            _gameFieldConfiguration = gameFieldConfiguration;
            _killedUnitsForNextLevel = GetCurrentLevelSettings().KilledUnitsForNextLevel;
        }
        public bool OnUnitsKill()
        {
            ++_killedUnitsInLevel;

            if (_killedUnitsInLevel < _killedUnitsForNextLevel)
            {
                return false;
            }
            IncrementCurrentLevel();
            _killedUnitsInLevel = 0;
            var settings = GetCurrentLevelSettings();
            _killedUnitsForNextLevel = settings.KilledUnitsForNextLevel;
            return true;
        }
        
        public GameFieldConfiguration.GameFieldLevelSettings GetCurrentLevelSettings()
        {
            return _gameFieldConfiguration.LevelSettings[_currentLevel];
        }
        private void IncrementCurrentLevel()
        {
            ++_currentLevel;
            var lastIndex = _gameFieldConfiguration.LevelSettings.Count - 1;
            if (_currentLevel >= lastIndex)
            {
                _currentLevel = lastIndex;
            }
        }
    }
}
