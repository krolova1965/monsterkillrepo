using System;
using System.Collections.Generic;
using Code.gameConfiguration;
using Code.Unit;
using UnityEngine;

namespace Code.GameField
{
    [CreateAssetMenu(order = 1, fileName = "GameConfiguration", menuName = "GameConfiguration")]
    public class GameFieldConfiguration : ScriptableObject
    {
        [SerializeField] private List<GameFieldLevelSettings> _levelSettings = new List<GameFieldLevelSettings>();
        public List<GameFieldLevelSettings> LevelSettings => _levelSettings;
       
       [Serializable]
       public class GameFieldLevelSettings
       {
           [SerializeField] private int _killedUnitsForNextLevel;
           [SerializeField] private List<UnitSettings> _unitSettings = new List<UnitSettings>();
           [SerializeField] private GameFieldSettings _gameFieldSettings;
           public List<UnitSettings> UnitSettings => _unitSettings;
           public GameFieldSettings GameFieldSettings => _gameFieldSettings;
           public int KilledUnitsForNextLevel => _killedUnitsForNextLevel;
       }
    }
}
