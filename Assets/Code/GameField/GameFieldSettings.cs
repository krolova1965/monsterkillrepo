using System;
using UnityEngine;

namespace Code.gameConfiguration
{
    [Serializable]
    public struct GameFieldSettings  
    {
        [SerializeField] private float _respawnTime; 
        [SerializeField] private int _unitsLimit;
        
        public float RespawnTime => _respawnTime;
        public int UnitsLimit => _unitsLimit;
        
    }
}
