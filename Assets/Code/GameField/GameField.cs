using System;
using System.Collections.Generic;
using Code.Unit;
using Code.Unit.Respawn;
using Code.Update;
using Zenject;
using Random = UnityEngine.Random;

namespace Code.GameField
{
    public class GameField: IUpdate, IDisposable
    {
        public event Action UnitKill;
        private UnitsCreator _unitsCreator;
        private RespawnZoneControl _respawnZoneControl;
        private ObeliskComponents _obeliskComponents; 
        private SpawnTimer _spawnTimer;
        private List<AiUnit> _aiUnits = new List<AiUnit>();
        private GameFieldLevelControl _gameFieldLevelControl;
        private int _unitsLimit;
        public SpawnTimer SpawnTimer => _spawnTimer;
        
        [Inject]
        public GameField(Updater updater, ObeliskComponents target, RespawnZoneControl respawnZoneControl, GameFieldConfiguration gameFieldConfiguration)
        {
            updater.Add(this);
            _obeliskComponents = target;
            _respawnZoneControl = respawnZoneControl;
            _gameFieldLevelControl = new GameFieldLevelControl(gameFieldConfiguration);  
            _unitsCreator = new UnitsCreator();
            _spawnTimer = new SpawnTimer();
            SetLevelData();
        }

        public void Update()
        {
            _spawnTimer.UpdateTimerAndRespawn(_aiUnits.Count, _unitsLimit, SpawnRandomUnit);
            for (int i = 0, len = _aiUnits.Count; i < len; ++i)
            {
                _aiUnits[i].Update();
            }
        }
        
        public void Dispose()
        {
            for (int i = 0, len = _aiUnits.Count; i < len; ++i)
            {
                _aiUnits[i].Death -= OnUnitDeath;
            }
        }

        public void KillAllUnits()
        {
            for (int i = _aiUnits.Count - 1; i >= 0; --i)
            {
                _aiUnits[i].Kill();
            }
        }
        private void SpawnRandomUnit()
        {
            var levelSettings = _gameFieldLevelControl.GetCurrentLevelSettings();
          
            var  randomIndex = Random.Range(0, levelSettings.UnitSettings.Count);
            var unit = _unitsCreator.GetPooledOrCreateNew(levelSettings.UnitSettings[randomIndex]);
            unit.SetObeliskComponents(_obeliskComponents);
            unit.SetMoveTarget();
            
            unit.Respawn();
            unit.UnitObj.transform.position = _respawnZoneControl.GetRandomPos();
            _aiUnits.Add(unit);
            unit.UnitObj.SetActive(true);
            unit.Death += OnUnitDeath;
        }
        
        private void OnUnitDeath(AiUnit unit)
        {
            UnitKill?.Invoke();
            unit.Death -= OnUnitDeath;
            _unitsCreator.UnitToPool(unit);
            _aiUnits.Remove(unit);
            
            if (!_gameFieldLevelControl.OnUnitsKill())
            {
                return;
            }
            SetLevelData();
        }

        private void SetLevelData()
        {
            var settings = _gameFieldLevelControl.GetCurrentLevelSettings();
            _unitsLimit = settings.GameFieldSettings.UnitsLimit;
            _spawnTimer.SetNewTimer(settings.GameFieldSettings.RespawnTime);
        }
    }
}
