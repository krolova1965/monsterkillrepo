using System;
using UnityEngine;

namespace Code.Damage
{
    public class DamageableObject : MonoBehaviour, IDamageable
    {
        public event Action<float> Hit;
        public void GetDamage(float damage)
        {
            Hit?.Invoke(damage);
        }
    }
}
