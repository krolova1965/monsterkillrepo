using Code.Animations;
using UnityEngine;

namespace Code.Damage
{
    public class DamageableAiObject : DamageableObject, IDamageable
    {
        [SerializeField] private UnitAnimatorControl _unitAnimatorControl;
        [SerializeField] private Collider _collider;
        public void OnHealthUpdated(float health)
        {
            _collider.enabled = health != 0;
            
            if (health == 0)
            {
                _unitAnimatorControl.Death();
               
                return;
            }
 
            _unitAnimatorControl.Hit();
        }
    }
}
