using UnityEngine;
using UnityEngine.UI;

namespace Code.Damage
{
    public class HealthView : MonoBehaviour
    {
        [SerializeField] private Image _healthBar;
        private float _maxValue;

        public void SetMaxValue(float max)
        {
            _maxValue = max;
        }
        public void OnHealthUpdate(float current)
        {
            _healthBar.fillAmount = current / _maxValue;
        }
    }
}
