namespace Code.Damage
{
    public interface IDamageable
    {
        void GetDamage(float value);
    }
}
