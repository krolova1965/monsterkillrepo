using System;
using Code.Unit;

namespace Code.Damage
{
    public class Health: IRestorable
    {
        public event Action<float> HealthUpdated;
        private float _health;
        private float _maxHealth;
        
        public void SetHealth(float maxHealth)
        {
            _maxHealth = maxHealth;
            _health = _maxHealth;
            HealthUpdated?.Invoke(_health);
        }
        public void HealthRemove(float value)
        {
            _health -= value;
            if (_health <= 0)
            {
                _health = 0;
            }
            HealthUpdated?.Invoke(_health);
        }
        public void HealthZero()
        {
            _health = 0;
            HealthUpdated?.Invoke(_health);
        }
        public void Restore()
        {
            _health = _maxHealth;
            HealthUpdated?.Invoke(_health);
        }
    }
}
