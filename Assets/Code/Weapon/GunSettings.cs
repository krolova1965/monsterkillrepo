using UnityEngine;

namespace Code.Weapon
{
    [CreateAssetMenu(order = 2, fileName = "GunSettings", menuName = "GunSettings")]
    public class GunSettings : ScriptableObject
    {
        [SerializeField] private float _startDamage; 
        [SerializeField] private float _maxDamage;
        [SerializeField] private float _startSpeed;
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _damageUpgradeValue;
        [SerializeField] private float _speedUpgradeValue;
        
        public float StartDamage => _startDamage; 
        public float MaxDamage => _maxDamage;
        public float StartSpeed => _startSpeed;
        public float MaxSpeed => _maxSpeed;
        public float DamageUpgradeValue => _damageUpgradeValue;
        public float SpeedUpgradeValue => _speedUpgradeValue;
    }
}
