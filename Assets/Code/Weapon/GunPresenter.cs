using System;
using Code.SimpleInput;
using Code.Windows;
using Zenject;

namespace Code.Weapon
{
    public class GunPresenter: IDisposable
    {
        private Gun _gun;
        private GunView _gunView;
        private InputControl _inputControl;
        private GunUpgradeWindow _gunUpgradeWindow;

        [Inject]
        public GunPresenter(Gun gun, GunView gunView, InputControl inputControl, 
            GunUpgradeWindow gunUpgradeWindow, GunSettings gunSettings)
        {
            _gun = gun;
            _inputControl = inputControl;
            _gunView = gunView;
            _gunUpgradeWindow = gunUpgradeWindow;
            _inputControl.ShootPerformed += _gun.GunShoot;
            _gun.Shoot += _gunView.Shoot;
            _gun.UpdateDamage += _gunUpgradeWindow.OnUpdateDamage;
            _gunUpgradeWindow.DamageUpgradeClick += _gun.UpgradeDamage;
            _inputControl.DirectionPerformed += _gunView.OnDirectionPerformed;
            _inputControl.RotateOnLeftPerformed += _gunView.OnRotateOnLeftPerformed;
            _inputControl.RotateOnRightPerformed += _gunView.OnRotateOnRightPerformed;
            _gun.SetData(gunSettings);
        }
        
        public void Dispose()
        {
            _inputControl.ShootPerformed -= _gun.GunShoot;
            _gun.Shoot -= _gunView.Shoot;
            _gun.UpdateDamage -= _gunUpgradeWindow.OnUpdateDamage;
            _gunUpgradeWindow.DamageUpgradeClick -= _gun.UpgradeDamage;
            _inputControl.DirectionPerformed -= _gunView.OnDirectionPerformed;
            _inputControl.RotateOnLeftPerformed -= _gunView.OnRotateOnLeftPerformed;
            _inputControl.RotateOnRightPerformed -= _gunView.OnRotateOnRightPerformed;
        }
    }
}
