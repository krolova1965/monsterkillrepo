using Code.Damage;
using UnityEngine;

namespace Code.Weapon
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private GameObject _effectPrefab;
        [SerializeField] private float _explosionRadius;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private float _force;
        [SerializeField] private float _upForce;
        private float _damage;

        public void AddForce(Vector3 direction)
        {
            _rigidbody.AddForce(transform.up * _upForce);
            _rigidbody.AddForce(direction * _force);
        }
        public void SetDamage(float value)
        {
            _damage = value;
        }
        private void OnCollisionEnter(Collision collision)
        {
            RadiusDamage(transform.position);
        }

        private void RadiusDamage(Vector3 location)
        {
            var objectsInRange = Physics.OverlapSphere(location, _explosionRadius);
            
            foreach (var col in objectsInRange)
            {
                var damageObj = col.GetComponent<DamageableAiObject>();
                if (damageObj != null)
                {
                    var proximity = (location - damageObj.transform.position).magnitude;
                    var effect = 1 - (proximity / _explosionRadius);

                    damageObj.GetDamage(_damage * effect);
                }
                Instantiate(_effectPrefab, location, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
