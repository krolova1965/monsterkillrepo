using Code.Utils;
using UnityEngine;

namespace Code.Weapon
{
     public class DestroyGo : MonoBehaviour
     {
          [SerializeField] private float _time;
          private Timer _timer;

          private void Awake()
          {
               _timer = new Timer(_time);
          }

          private void Update()
          {
               _timer.UpdateTimer();
               if (_timer.available)
               {
                    Destroy(gameObject);
               }
          }
     }
}
