using System;

namespace Code.Weapon
{
    public class Gun
    {
        public event Action<float> Shoot;
        public event Action<float> UpdateDamage;
        public event Action<float> UpdateSpeed;
        private float _maxDamage; 
        private float _maxShootSpeed; 
        private float _damage;
        private float _shootSpeed;
        private float _damageUpgradeValue;
        private float _speedUpgradeValue;
        public void SetData(GunSettings gunSettings)
        {
            _damage = gunSettings.StartDamage;
            _shootSpeed = gunSettings.StartSpeed;
            _maxDamage = gunSettings.MaxDamage;
            _maxShootSpeed = gunSettings.MaxSpeed;
            _damageUpgradeValue = gunSettings.DamageUpgradeValue;
            _speedUpgradeValue = gunSettings.SpeedUpgradeValue;
            UpdateDamage?.Invoke(_damage);
        }

        public void GunShoot()
        {
            Shoot?.Invoke(_damage);
        }
        public void UpgradeDamage()
        {
            _damage = Sum(_damage, _damageUpgradeValue, _maxDamage);
            UpdateDamage?.Invoke(_damage);
        }
        public void UpgradeShootSpeed( )
        {
            _shootSpeed = Sum(_shootSpeed, _speedUpgradeValue, _maxShootSpeed);
            UpdateSpeed?.Invoke(_shootSpeed);
        }
        private float Sum(float value, float addedValue, float maxValue)
        {
            value += addedValue;
            if (value > maxValue)
            {
                value = maxValue;
            }

            return value;
        }
    }
}
