using UnityEngine;

namespace Code.Weapon
{
    public class GunView : MonoBehaviour
    {
        [SerializeField] private Projectile _projectilePrefab;
        [SerializeField] private Camera _camera;
        [SerializeField] private Transform firePoint;
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _rotateSpeed;

        public void Shoot(float damage)
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            var  projectile = Instantiate(_projectilePrefab, firePoint.transform.position, Quaternion.identity);
            projectile.SetDamage(damage);
            projectile.AddForce(ray.direction);
        }

        public void OnDirectionPerformed(Vector2 direction)
        {
            transform.position += transform.forward * direction.y * _moveSpeed * Time.deltaTime;
            transform.position += transform.right * direction.x * _moveSpeed * Time.deltaTime;
        }

        public void OnRotateOnLeftPerformed()
        {
            Rotate(-1);
        }

        public void OnRotateOnRightPerformed()
        {
            Rotate(1);
        }
        private void Rotate(int rightLeftValue)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 
                transform.rotation.eulerAngles.y + _rotateSpeed * rightLeftValue * Time.deltaTime , transform.rotation.eulerAngles.z);
        }
    }
}
