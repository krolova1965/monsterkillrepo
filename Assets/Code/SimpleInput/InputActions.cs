using System;
using UnityEngine;

namespace Code.SimpleInput
{
    [CreateAssetMenu(order = 1, fileName = "InputActions", menuName = "InputActions")]
    public class InputActions : ScriptableObject
    {
        public InputAction shootAction;
        public AxisAction verticalAction;
        public AxisAction horizontalAction;
        public InputAction rotateOnLeft;
        public InputAction rotateOnRight;
    }
    
    [Serializable]
    public struct InputAction
    {
        public KeyCode button;
    }

    [Serializable]
    public struct AxisAction
    {
        public string name;
    }
    
}
