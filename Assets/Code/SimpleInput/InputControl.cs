using System;
using Code.Update;
using UnityEngine;

namespace Code.SimpleInput
{
    public class InputControl: IUpdate
    {
        public event Action ShootPerformed;
        public event Action RotateOnLeftPerformed;
        public event Action RotateOnRightPerformed;
        public event Action<Vector2> DirectionPerformed;
        private InputActions _inputActions;

        public InputControl(Updater updater, InputActions inputActions)
        {
            updater.Add(this);
            _inputActions = inputActions;
        }
        public void Update()
        {
            if(Input.GetKeyDown(_inputActions.shootAction.button))
            {
                ShootPerformed?.Invoke();
            }

            var direction = new Vector2(Input.GetAxis(_inputActions.horizontalAction.name),
                Input.GetAxis(_inputActions.verticalAction.name));

            if (direction.x != 0 || direction.y != 0)
            {
                DirectionPerformed?.Invoke(direction);
            }

            if (Input.GetKey(_inputActions.rotateOnLeft.button))
            {
                RotateOnLeftPerformed?.Invoke();
            }
            if (Input.GetKey(_inputActions.rotateOnRight.button))
            {
                RotateOnRightPerformed?.Invoke();
            }
        }
    }
}
