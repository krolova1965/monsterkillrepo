using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Code.Animations
{
    public class AnimationsEventsControl : MonoBehaviour
    {
        public event Action AnimationActionStart;
        public event Action AnimationActionEnd;
        
        [UsedImplicitly]
        public void AnimationActionStartInvoke()
        {
            AnimationActionStart?.Invoke();
        }
        
        [UsedImplicitly]
        public void AnimationActionEndInvoke()
        {
            AnimationActionEnd?.Invoke();
        }
    }
}
