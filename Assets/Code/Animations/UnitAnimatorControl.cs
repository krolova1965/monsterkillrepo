using UnityEngine;

namespace Code.Animations
{
    public class UnitAnimatorControl: MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _walkTransitionName;
        [SerializeField] private string _AttackTransitionName;
        [SerializeField] private string _hitTransitionName;
        [SerializeField] private string _deathTransitionName;

        public void SetWalk(bool value)
        {
            _animator.SetBool(_walkTransitionName, value);
        }
        public void SetAttack()
        {
            _animator.SetTrigger(_AttackTransitionName);
        }
        public void Hit()
        {
            _animator.SetTrigger(_hitTransitionName);
        }

        public void Death()
        {
            _animator.SetTrigger(_deathTransitionName);
        }
    }
}
