using System;
using UnityEngine;

namespace Code.Unit
{
    [Serializable]
    public struct UnitSettings 
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _maxHealth;
        [SerializeField] private float _damage;
        [SerializeField] private float _attackTime;
        [SerializeField] private GameObject _unitPrefab;
        [SerializeField] private UnitType _unitType;

        public float Speed => _speed;
        public float MaxHealth => _maxHealth;
        public float Damage => _damage;
        public float AttackTime => _attackTime;
        public GameObject UnitPrefab => _unitPrefab;
        public UnitType UnitType => _unitType;
    }

    public enum UnitType
    {
        none,
        skelet, 
        demon
    }
}
