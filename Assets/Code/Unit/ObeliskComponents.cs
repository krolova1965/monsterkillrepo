using Code.Damage;
using Code.Obelisk;
using UnityEngine;

namespace Code.Unit
{
    public class ObeliskComponents : MonoBehaviour
    {
        [SerializeField] private HealthView _healthView;

        [SerializeField] private ObeliskUnitsPoints _obeliskUnitsPoints;

        [SerializeField] private DamageableObject _damageableObject;
        public HealthView HealthView =>_healthView;
        public ObeliskUnitsPoints ObeliskUnitsPoints => _obeliskUnitsPoints;
        public DamageableObject DamageableObject => _damageableObject;
    }
}
