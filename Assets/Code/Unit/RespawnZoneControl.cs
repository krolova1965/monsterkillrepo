using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.Unit
{
    public class RespawnZoneControl: MonoBehaviour
    {
        [SerializeField] private Vector3 _respawnFieldSize;
        [SerializeField] private Vector3 _center;
        
        public Vector3 GetRandomPos()
        {
            var pos = _center + new Vector3(
                Random.Range(-_respawnFieldSize.x / 2, _respawnFieldSize.x / 2),
                Random.Range(-_respawnFieldSize.y / 2, _respawnFieldSize.y / 2),
                Random.Range(-_respawnFieldSize.z / 2, _respawnFieldSize.z / 2));
            return pos;
            
        }
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1, 1, 0, 0.5f);
            Gizmos.DrawCube(transform.localPosition + _center, _respawnFieldSize);
        }
    }
}
