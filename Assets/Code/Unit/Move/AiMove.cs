using System;
using Code.Animations;
using Code.Obelisk;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Unit.Move
{
    public class AiMove: IRestorable
    {
        public event Action<bool> TargetReached;
        private UnitAnimatorControl _unitAnimatorControl;
        private NavMeshAgent _navMeshAgent;
        private Transform _unitTransform;
        private UnitPoint _unitPoint;

        public AiMove(GameObject unitObj)
        {
            _unitTransform = unitObj.transform;
            _navMeshAgent = unitObj.GetComponent<NavMeshAgent>();
            _unitAnimatorControl = unitObj.GetComponent<UnitAnimatorControl>();
        }
        
        public void SetUnitPoint(UnitPoint unitPoint)
        {
            _unitPoint = unitPoint;
            if (_unitPoint == null)
            {
                return;
            }
            _unitPoint.TakePoint(true);
        }

        public void StopEnable()
        {
            _navMeshAgent.isStopped = true;
        }
        
        public void StopDisable()
        {
            _navMeshAgent.isStopped = false;
        }
        public void SetSpeed(float speed)
        {
            _navMeshAgent.speed = speed;
        }

        public void Update()
        {
            if (_unitPoint == null)
            { 
                _unitAnimatorControl.SetWalk(false);
                return;
            }
            _navMeshAgent.SetDestination(_unitPoint.transform.position);
            var reached = DestinationReached();
            if (reached)
            {
                RotateOnDir();
            }
            _unitAnimatorControl.SetWalk(!reached);
            TargetReached?.Invoke(reached);
        }
        
        public void Restore()
        {
            TargetReached?.Invoke(false);
        }

        public void RemoveUnitPoint()
        {
            if (_unitPoint == null)
            {
                return;
            }
            _unitPoint.TakePoint(false);
            _unitPoint = null;
        }

        private void RotateOnDir()
        {
            _unitTransform.rotation = Quaternion.LookRotation(_unitPoint.transform.forward);
        }
        private bool DestinationReached()
        {
            if (_unitPoint == null)
            {
                return false;
            }
            return (_unitPoint.transform.position - _unitTransform.position).magnitude <= _navMeshAgent.stoppingDistance;
        }
    }
}
