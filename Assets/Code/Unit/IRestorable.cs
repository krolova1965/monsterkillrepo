namespace Code.Unit
{
    public interface IRestorable
    {
        public void Restore();
    }
}
