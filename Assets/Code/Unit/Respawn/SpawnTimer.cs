using System;
using Code.Utils;

namespace Code.Unit.Respawn
{
    public class SpawnTimer
    {
        public event Action<float> TimerUpdate; 
        private Timer _timer;
        private bool _respawnStopped;
        
        public void SetNewTimer(float respawnTime)
        {
            _timer = new Timer(respawnTime);
        }
        public void SetRespawnStopped(bool value)
        {
            _respawnStopped = value;
        }
        
        public void UpdateTimerAndRespawn(int unitsAmount, int unitsLimit, Action spawn)
        {
            if (unitsAmount == unitsLimit || _respawnStopped)
            {
                return;
            }
            
            var timerValue =_timer.UpdateTimer();
            TimerUpdate?.Invoke(timerValue);
            
            if (!_timer.available)
            {
                return;
            }

            for (var i = 0; i < unitsLimit - unitsAmount; ++i)
            {
                spawn?.Invoke();
                _timer.TimerZero();
            }
        }
    }
}
