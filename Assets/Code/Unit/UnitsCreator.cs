using System.Collections.Generic;
using UnityEngine;
using UnitSettings = Code.Unit.UnitSettings;

namespace Code.Unit 
{
    public class UnitsCreator
    {
        private List<AiUnit> _pooledUnits = new List<AiUnit>();
        
        public AiUnit GetPooledOrCreateNew(UnitSettings unitSettings)
        {
            for (int i = 0, len = _pooledUnits.Count; i < len; ++i)
            {
                var unit = _pooledUnits[i];
               
                if (unit.UnitType == unitSettings.UnitType)
                {
                    unit.SetSettings(unitSettings);
                    _pooledUnits.Remove(unit);
                    return unit;
                }
            }

            return CreateNewUnit(unitSettings);
        }

        public void UnitToPool(AiUnit aiUnit)
        {
            _pooledUnits.Add(aiUnit);
            aiUnit.UnitObj.SetActive(false);
        }

        private AiUnit CreateNewUnit(UnitSettings unitSettings)
        {
            var obj = Object.Instantiate(unitSettings.UnitPrefab);
            obj.SetActive(false);
            var unit = new AiUnit(obj);
            unit.SetSettings(unitSettings);
            return unit;
        }
    }
}
