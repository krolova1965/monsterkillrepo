using System;
using Code.Animations;
using Code.Damage;
using Code.Unit.Attack;
using Code.Unit.Move;
using UnityEngine;

namespace Code.Unit
{
    public class AiUnit: IDisposable
    {
        public event Action<AiUnit> Death;
        
        private AiMove _aiMove;
        private Health _health;
        private AiAttack _aiAttack;

        private DamageableAiObject _damageableAiObject;
        private AiAttackView _aiAttackView;
        private AnimationsEventsControl _animationsEventsControl;
        private GameObject _unitObj;
        private UnitType _unitType;
        private ObeliskComponents _obeliskComponents;
        
        public GameObject UnitObj => _unitObj;
        public UnitType UnitType => _unitType;
        
        public AiUnit(GameObject unitObj)
        {
            _unitObj = unitObj;
            _animationsEventsControl = unitObj.GetComponent<AnimationsEventsControl>();
            _damageableAiObject = unitObj.GetComponent<DamageableAiObject>();
            _aiAttackView = unitObj.GetComponent<AiAttackView>();
            
            _health = new Health();
            _aiMove = new AiMove(unitObj);
            _aiAttack = new AiAttack();

            Subscribe();
        }

        public void SetSettings(UnitSettings unitSettings)
        {
            _unitType = unitSettings.UnitType;
            _health.SetHealth(unitSettings.MaxHealth);
            _aiMove.SetSpeed(unitSettings.Speed);
            _aiAttack.SetSettings(unitSettings.AttackTime, unitSettings.Damage);
        }
        public void Update()
        {
            _aiMove.Update();
            _aiAttack.Update();
        }

        public void Dispose()
        {
            UnSubscribe();
        }

        public void Respawn()
        {
            _health.Restore();
            _aiMove.Restore();
        }

        public void SetObeliskComponents(ObeliskComponents obeliskComponents)
        {
            _obeliskComponents = obeliskComponents;
            _aiAttack.SetAttackTarget(obeliskComponents.DamageableObject);
        }

        public void SetMoveTarget()
        {
            _aiMove.SetUnitPoint(_obeliskComponents.ObeliskUnitsPoints.GetFreePoint());
        }

        public void Kill()
        {
            _health.HealthZero();
        }

        private void OnHealthUpdate(float health)
        {
            if (health <= 0)
            {
                Death?.Invoke(this);
                _aiMove.RemoveUnitPoint();
            }
        }

        private void Subscribe()
        {
            _health.HealthUpdated += _damageableAiObject.OnHealthUpdated;
            _damageableAiObject.Hit +=_health.HealthRemove;
            _aiAttack.StartAttack += _aiAttackView.OnStartAttack;
            _aiMove.TargetReached += _aiAttack.OnReachTarget;
            _animationsEventsControl.AnimationActionStart += _aiMove.StopEnable;
            _animationsEventsControl.AnimationActionEnd += _aiMove.StopDisable;
            _animationsEventsControl.AnimationActionEnd += _aiAttack.DamageOnAttackAnimationEnd;
            _health.HealthUpdated += OnHealthUpdate;
        }
        
        private void UnSubscribe()
        {
            _health.HealthUpdated -= _damageableAiObject.OnHealthUpdated;
            _damageableAiObject.Hit -= _health.HealthRemove;
            _aiAttack.StartAttack -= _aiAttackView.OnStartAttack;
            _aiMove.TargetReached -= _aiAttack.OnReachTarget;
            _animationsEventsControl.AnimationActionStart -= _aiMove.StopEnable;
            _animationsEventsControl.AnimationActionEnd -= _aiMove.StopDisable;
            _animationsEventsControl.AnimationActionEnd -= _aiAttack.DamageOnAttackAnimationEnd;
            _health.HealthUpdated -= OnHealthUpdate;
        }
    }
}
