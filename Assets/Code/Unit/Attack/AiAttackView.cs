using Code.Animations;
using UnityEngine;

namespace Code.Unit.Attack
{
    public class AiAttackView : MonoBehaviour
    {
        [SerializeField] private UnitAnimatorControl _unitAnimatorControl;
        public void OnStartAttack(float damage)
        {
            _unitAnimatorControl.SetAttack();
        }
    }
}
