using System;
using Code.Damage;
using Timer = Code.Utils.Timer;

namespace Code.Unit.Attack
{
    public class AiAttack
    {
        public event Action<float> StartAttack;
        private Timer _timer;
        private float _damage;
        private bool _targetReached;
        private DamageableObject _targetToDamage;
        
        public void SetSettings(float attackTime, float damage)
        {
            _timer = new Timer(attackTime);
            _damage = damage;
        }

        public void OnReachTarget(bool value)
        {
            _targetReached = value;
        }

        public void Update()
        {
            if (!_targetReached)
            {
                return;
            }
            _timer.UpdateTimer();
            
            if (_timer.available)
            {
                StartAttack?.Invoke(_damage);
                _timer.TimerZero();
            }
        }
        public void SetAttackTarget(DamageableObject targetToDamage)
        {
            _targetToDamage = targetToDamage;
        }
        public void DamageOnAttackAnimationEnd()
        {
            _targetToDamage.GetDamage(_damage);
        }
    }
}
