using UnityEngine.SceneManagement;

namespace Code.Scene
{
    public static class ScenesControl
    {
        public static void LoadGameScene()
        {
            SceneManager.LoadScene(1);
        }

        public static void LoadMenu()
        {
            SceneManager.LoadScene(0);
        }
    }
}
