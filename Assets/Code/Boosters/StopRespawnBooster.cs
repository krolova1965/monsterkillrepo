using Code.Utils;

namespace Code.Boosters
{
    public class StopRespawnBooster: Booster
    {
        private float _workTime;
        private Timer _timer;

        public StopRespawnBooster(GameField.GameField gameField, float workTime) : base(gameField)
        {
            _workTime = workTime;
            _timer = new Timer(_workTime);
        }
        public override bool Execute()
        {
            _timer.UpdateTimer();
            if (!_timer.available)
            {
                _gameField.SpawnTimer.SetRespawnStopped(true);
                return false;
            }
            _gameField.SpawnTimer.SetRespawnStopped(false);
            _timer.TimerZero();
            return true;
        }
    }
}
