using System;
using UnityEngine;

namespace Code.Boosters
{
    public class BoosterButton : MonoBehaviour
    {
        public event Action<BoostersTypes, float, float> BoosterClicked;
        [SerializeField] private BoostersTypes _boostersTypes;
        [SerializeField] private float _price;
        [SerializeField] private float _workTime; 
        public float Price => _price;
        public float WorkTime => _workTime;
        
        public void OnBoosterClick()
        {
            BoosterClicked?.Invoke(_boostersTypes, _price, _workTime);
        }
    }
}
