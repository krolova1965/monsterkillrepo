namespace Code.Boosters
{
    public class KillAllBooster : Booster
    {
        public KillAllBooster(GameField.GameField gameField) : base(gameField)
        {
        }
        public override bool Execute()
        {
            _gameField.KillAllUnits();
            return true;
        }
    }
}
