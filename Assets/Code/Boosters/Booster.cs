namespace Code.Boosters
{
    public abstract class Booster 
    {
        protected float _price;
        protected GameField.GameField _gameField;
        public float Price => _price;

        public Booster(GameField.GameField gameField)
        {
            _gameField = gameField;
        }
        public abstract bool Execute();
    }
}
