using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Boosters.BoostersControl
{
    public class BoostersControlView : MonoBehaviour
    {
        public event Action<BoostersTypes, float, float> BoosterFired;
        [SerializeField] private Image _userPointsBar;
        [SerializeField] private BoosterButton[] _boosters;

        public void OnPointsCountUpdate(float value, float maxValue)
        {
            _userPointsBar.fillAmount = value / maxValue;
            BustersSetActive(value);
        }

        public void OnDisable()
        {
            for (int i = 0, len = _boosters.Length; i < len; ++i)
            {
                _boosters[i].BoosterClicked -= OnBoosterClick;
            }
        }

        private void Awake()
        {
            for (int i = 0, len = _boosters.Length; i < len; ++i)
            {
                _boosters[i].BoosterClicked += OnBoosterClick;
            }
        }
        
        private void OnBoosterClick(BoostersTypes boostersTypes, float price, float workTime)
        {
            BoosterFired?.Invoke(boostersTypes, price, workTime);
        }

        private void BustersSetActive(float pointsCount)
        {
            for (int i = 0, len = _boosters.Length; i < len; ++i)
            {
                _boosters[i].gameObject.SetActive(pointsCount >= _boosters[i].Price);
            }
        }
    }
}
