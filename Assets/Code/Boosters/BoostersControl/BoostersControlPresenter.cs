using System;
using System.Collections.Generic;
using Code.Update;
using Zenject;

namespace Code.Boosters.BoostersControl
{
    public class BoostersControlPresenter : IDisposable, IUpdate
    {
        private BoostersControlModel _boostersControlModel;
        private BoostersControlView _boostersControlView;
        private GameField.GameField _gameField;
        private List<Booster> _boosters = new List<Booster>();

        [Inject]
        public BoostersControlPresenter(Updater updater, BoostersControlModel boostersControlModel,
            BoostersControlView boostersControlView, GameField.GameField gameField)
        {
            updater.Add(this);
            _boostersControlModel = boostersControlModel;
            _boostersControlView = boostersControlView;
            _gameField = gameField;
            _gameField.UnitKill += _boostersControlModel.IncrementPointsCount;
            _boostersControlModel.PointsCountUpdate += _boostersControlView.OnPointsCountUpdate;
            _boostersControlView.BoosterFired += OnBoosterFired;
        }

        public void Update()
        {
            for (int i = 0, len = _boosters.Count; i < len; ++i)
            {
                if (_boosters[i].Execute())
                {
                    _boosters.Remove(_boosters[i]);
                }
            }
        }
        
        public void Dispose()
        {
            _gameField.UnitKill -= _boostersControlModel.IncrementPointsCount;
            _boostersControlModel.PointsCountUpdate -= _boostersControlView.OnPointsCountUpdate;
            _boostersControlView.BoosterFired -= OnBoosterFired;
        }

        private void OnBoosterFired(BoostersTypes boostersTypes, float price, float workTime)
        {
            _boostersControlModel.SpendPoints(price);
            _boosters.Add(CreateBooster(boostersTypes, workTime));
        }

        private Booster CreateBooster(BoostersTypes boostersTypes, float workTime)
        {
            switch (boostersTypes)
            {
                case BoostersTypes.StopRespawn:
                   return new StopRespawnBooster(_gameField, workTime);

                case BoostersTypes.KillAll:
                    return new KillAllBooster(_gameField);
            }

            return null;
        }
    }
}
