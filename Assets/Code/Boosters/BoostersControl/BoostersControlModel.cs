using System;
using Zenject;

namespace Code.Boosters.BoostersControl
{
    public class BoostersControlModel
    {
        public event Action<float, float> PointsCountUpdate;
        private float _userPointsCount;
        private float _userPointsCountMax;
        
        [Inject]
        public BoostersControlModel(BoostersControlSettings boostersControlSettings)
        {
            _userPointsCountMax = boostersControlSettings.UserPointsMaxCount;
        }
        
        public void IncrementPointsCount()
        {
            ++_userPointsCount;
            if (_userPointsCount > _userPointsCountMax)
            {
                _userPointsCount = _userPointsCountMax;
            }
            PointsCountUpdate?.Invoke(_userPointsCount, _userPointsCountMax);
        }

        public void SpendPoints(float price)
        {
            _userPointsCount -= price;
            PointsCountUpdate?.Invoke(_userPointsCount, _userPointsCountMax);
        }
    }
}
