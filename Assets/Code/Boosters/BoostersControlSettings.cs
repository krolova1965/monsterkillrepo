using UnityEngine;

namespace Code.Boosters
{
    [CreateAssetMenu(order = 2, fileName = "BoostersControlSettings", menuName = "BoostersControlSettings")]
    public class BoostersControlSettings : ScriptableObject
    {
       [SerializeField] private float _userPointsMaxCount = 10;
       public float UserPointsMaxCount => _userPointsMaxCount;
    }
}
