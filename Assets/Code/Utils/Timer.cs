using UnityEngine;

namespace Code.Utils
{
    public class Timer
    {
        public bool available;
        private float _timer;
        private readonly float _time;
        public Timer(float time)
        {
            _time = time;
        }

        public float UpdateTimer()
        {
            available = _timer >= _time;
            if (_timer >= _time)
            {
                _timer = _time;
                return _timer;
            }

            return _timer += Time.deltaTime;
        }

        public void TimerZero()
        {
            _timer = 0;
        }
    }
}
