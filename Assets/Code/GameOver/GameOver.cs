using System;
using Code.Windows;

namespace Code.GameOver
{
    public class GameOver : IDisposable
    {
        private GameOverWindow _gameOverWindow;
        private Obelisk.Obelisk _obelisk;

        public GameOver(GameOverWindow gameOverWindow, Obelisk.Obelisk obelisk)
        {
            _gameOverWindow = gameOverWindow;
            _obelisk = obelisk;
            _obelisk.Health.HealthUpdated += OnHealthRemove;
        }
        
        public void Dispose()
        {
            _obelisk.Health.HealthUpdated -= OnHealthRemove;
        }

        private void OnHealthRemove(float health)
        {
            if (health == 0)
            {
                _gameOverWindow.Open();
            }
        }
    }
}
