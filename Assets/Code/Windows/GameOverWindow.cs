using Code.Scene;

namespace Code.Windows
{
    public class GameOverWindow : Window
    {
        public void OnRestartButtonClick()
        {
            ScenesControl.LoadGameScene();
            Close();
        }
    }
}
