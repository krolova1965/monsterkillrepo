using Code.Scene;
using UnityEngine;

namespace Code.Windows
{
    public class GameMenuWindow : Window
    {
        [SerializeField] private ScoreLeadersWindow scoreLeadersWindow;
        [SerializeField] private Window _creditsWindow;
        
        public void OnStartGameClick()
        {
            ScenesControl.LoadGameScene();
        }

        public void OnScoresButtonClick()
        {
            scoreLeadersWindow.gameObject.SetActive(true);
        }

        public void OnCreditsButtonClick()
        {
            _creditsWindow.gameObject.SetActive(true);
        }

        public void OnQuitButtonClick()
        {
            Application.Quit();
        }
    }
}
