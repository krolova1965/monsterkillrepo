using Code.Scene;
using TMPro;
using UnityEngine;
using Zenject;

namespace Code.Windows
{
    public class UserWindow : Window
    {
        [SerializeField] private TMP_Text _respawnTime;
        [SerializeField] private GunUpgradeWindow _gunUpgradeWindow;
        private GameField.GameField _gameField;

        [Inject]
        public void Init(GameField.GameField gameField)
        {
            _gameField = gameField;
            _gameField.SpawnTimer.TimerUpdate += OnRespawnTimerUpdate;
        }
        
        public void UpgradeButtonClick()
        {
            _gunUpgradeWindow.EnableWindow(true);
        }

        public void OnMenuClick()
        {
            ScenesControl.LoadMenu();
        }
        private void OnRespawnTimerUpdate(float value)
        {
            _respawnTime.text = value.ToString();
        }
        
        private void OnDestroy()
        {
            _gameField.SpawnTimer.TimerUpdate -= OnRespawnTimerUpdate;
        }
    }
}
