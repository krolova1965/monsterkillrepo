using UnityEngine;

namespace Code.Windows
{
    public class Window : MonoBehaviour
    {
        public void Close()
        {
            gameObject.SetActive(false);
        }

        public void Open()
        {
            gameObject.SetActive(true);
        }
    }
}
