using System;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace Code.Windows
{
    public class GunUpgradeWindow : Window
    {
        public event Action DamageUpgradeClick;
        [SerializeField] private TMP_Text _damageValueText;
        
        public void EnableWindow(bool value)
        {
            gameObject.SetActive(value);
        }
        public void OnUpdateDamage(float damage)
        {
            _damageValueText.text = damage.ToString();
        }

        [UsedImplicitly]
        public void OnDamageUpdateButtonClick()
        {
            DamageUpgradeClick?.Invoke();
        }
    }
}
