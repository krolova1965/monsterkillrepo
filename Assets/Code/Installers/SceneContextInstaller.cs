using Code.Boosters;
using Code.Boosters.BoostersControl;
using Code.gameConfiguration;
using Code.GameField;
using Code.Obelisk;
using Code.SimpleInput;
using Code.Unit;
using Code.Update;
using Code.Weapon;
using Code.Windows;
using UnityEngine;
using Zenject;

namespace Code.Installers
{
    public class SceneContextInstaller : MonoInstaller
    {
        [SerializeField] private Updater _updater;
        [SerializeField] private ObeliskComponents obeliskComponents;
        [SerializeField] private RespawnZoneControl _respawnZoneControl;
        [SerializeField] private InputActions _inputActions;
        [SerializeField] private GunSettings _gunSettings;
        [SerializeField] private GunView _gunView;
        [SerializeField] private UserWindow _userWindow;
        [SerializeField] private BoostersControlSettings _boostersControlSettings;
        [SerializeField] private GameFieldConfiguration gameFieldConfiguration;
        [SerializeField] private GunUpgradeWindow _gunUpgradeWindow;
        [SerializeField] private BoostersControlView _boostersControlView;
        [SerializeField] private ObeliskSettings _obeliskSettings;
        [SerializeField] private GameOverWindow _gameOverWindow;
        
        public override void InstallBindings()
        {
            Container.BindInstance(_updater).AsSingle().NonLazy();
            Container.BindInstance(_inputActions).AsSingle().NonLazy();
            Container.Bind<InputControl>().AsSingle().NonLazy();
            Container.BindInstance(obeliskComponents).AsSingle().NonLazy();
            Container.BindInstance(_respawnZoneControl).AsSingle().NonLazy();
            InstallGun();
            InstallGameField();
            InstallBoostersControl();
            InstallObelisk();
            InstallGameOver();
            Container.BindInstance(_userWindow).AsSingle().NonLazy();
        }

        private void InstallGun()
        {
            Container.Bind<Gun>().AsSingle().NonLazy();
            Container.BindInstance(_gunSettings).AsSingle().NonLazy();
            Container.BindInstance(_gunView).AsSingle().NonLazy();
            Container.BindInstance(_gunUpgradeWindow).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GunPresenter>().AsSingle().NonLazy();
        }

        private void InstallGameField()
        {
            Container.BindInstance(gameFieldConfiguration).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GameField.GameField>().AsSingle().NonLazy();
        }
        private void InstallBoostersControl()
        {
            Container.BindInstance(_boostersControlSettings).AsSingle().NonLazy();
            Container.Bind<BoostersControlModel>().AsSingle().NonLazy();
            Container.BindInstance(_boostersControlView).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<BoostersControlPresenter>().AsSingle().NonLazy();
        }

        private void InstallObelisk()
        {
            Container.BindInstance(_obeliskSettings).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<Obelisk.Obelisk>().AsSingle().NonLazy();
        }

        private void InstallGameOver()
        {
            Container.BindInstance(_gameOverWindow).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GameOver.GameOver>().AsSingle().NonLazy();
        }
    }
}