using UnityEngine;

namespace Code.Obelisk
{
    public class UnitPoint : MonoBehaviour
    {
        [SerializeField] private bool _isBusy;
        public bool IsBusy => _isBusy;

        public void TakePoint(bool value)
        {
            _isBusy = value;
        }
    }
}
