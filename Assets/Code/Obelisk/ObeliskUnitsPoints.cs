using UnityEngine;

namespace Code.Obelisk
{
    public class ObeliskUnitsPoints : MonoBehaviour
    {
        [SerializeField] private UnitPoint[] _points;
        
        public UnitPoint GetFreePoint()
        {
            UnitPoint point = null;
            for (int i = 0, len = _points.Length; i < len; ++i)
            {
                if (_points[i].IsBusy)
                {
                    continue;
                }

                point = _points[i];
            }

            return point;
        }
    }
}
