using UnityEngine;

namespace Code.Obelisk
{
    [CreateAssetMenu(order = 1, fileName = "ObeliskSettings", menuName = "ObeliskSettings")]
    public class ObeliskSettings : ScriptableObject
    {
        [SerializeField] private float _health;
        public float Health => _health;
    }
}
