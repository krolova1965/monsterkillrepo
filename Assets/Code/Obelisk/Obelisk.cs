using System;
using Code.Damage;
using Code.Unit;
using Zenject;

namespace Code.Obelisk
{
    public class Obelisk: IDisposable
    {
        private HealthView _healthView;
        private DamageableObject _damageableObject;
        private Health _health;
        public Health Health => _health;

        [Inject]
        public Obelisk(ObeliskSettings obeliskSettings, ObeliskComponents obeliskComponents)
        {
            _healthView = obeliskComponents.HealthView;
            _health = new Health();
            _damageableObject = obeliskComponents.gameObject.GetComponent<DamageableObject>();
            _healthView.SetMaxValue(obeliskSettings.Health);
            _health.HealthUpdated += _healthView.OnHealthUpdate;
            _damageableObject.Hit += _health.HealthRemove; 
            _health.SetHealth(obeliskSettings.Health);
        }

        public void Dispose()
        {
            _health.HealthUpdated -= _healthView.OnHealthUpdate;
            _damageableObject.Hit -= _health.HealthRemove;
        }
    }
}
